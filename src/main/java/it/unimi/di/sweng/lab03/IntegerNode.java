package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private Integer value=null;
	private IntegerNode next=null;
	
	public IntegerNode(int value) {
		// TODO Auto-generated constructor stub
		this.value=value;
	}

	public IntegerNode next() {
		// TODO Auto-generated method stub
		return next;
	}

	public void setNext(IntegerNode integerNode) {
		// TODO Auto-generated method stub
		next = integerNode;
	}

	public String getValue() {
		// TODO Auto-generated method stub
		return value.toString();
	}
	
}
