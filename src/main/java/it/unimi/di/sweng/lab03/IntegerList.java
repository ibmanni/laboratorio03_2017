package it.unimi.di.sweng.lab03;

public class IntegerList {
	private IntegerNode head;
	private IntegerNode tail;
	
	public IntegerList(){
		head = null;
		tail = null;
	}
	
	public IntegerList(String listinput) {
		
		String[] listanumber= listinput.split(" ");
		for(int i=0;i<listanumber.length;i++)
			if(isNumber(listanumber[i]))
				addLast(Integer.valueOf(listanumber[i]));
			else
				throw new IllegalArgumentException("Argomento non valido");
	}

	@Override
	public String toString() {
		StringBuilder result=new StringBuilder("[");
		IntegerNode currentNode =head;
		int i=0;
		while(currentNode!=null){
			if(i++>0)
				result.append(' ');
			result.append(currentNode.getValue());
			currentNode=currentNode.next();
		}
		result.append(']');
		return result.toString();
	}

	public void addLast(int value) {
		
		if(head==null){
			head = tail = new IntegerNode(value);
		} else{
			IntegerNode node = new IntegerNode(value);
			tail.setNext(node);
			tail = node;
		}
	}
	private static boolean isNumber(String dato){
		for(int i=0;i<dato.length();i++)
			if(Character.isLetter(dato.charAt(i)))
				return false;
		return true;
	}
}
